#include "generators.h"

typedef struct s_reflow_data {
	gchar *in;
	gchar *out;
} reflow_data;

reflow_data reflow[4] = {
		{ "ww   w", "ww w " },
		{ "    ww w", "ww w " },
		{ "ww w    ", "ww w " },
		{ " ww  w   ", "ww w " }
};

/*
 * gunichar * generators_utf8_to_ucs4_reflow(gchar *text, glong *ucs4_size) {
 */
void test_generators_utf8_to_ucs4_reflow(gconstpointer user_data) {
	reflow_data *data = (reflow_data*) user_data;
	glong size;
	for (int i = 0; i < 4; i++) {
		gunichar *ucs4 = generators_utf8_to_ucs4_reflow(data->in,
				&size);
		gchar *out = g_ucs4_to_utf8(ucs4, size, NULL, NULL, NULL);
		g_assert_cmpstr(data->out, ==, out);
	}
}

int main(int argc, char **argv) {
	g_test_init(&argc, &argv, NULL);

	g_test_add_data_func("/generators/generators_utf8_to_ucs4_reflow",
			reflow, &test_generators_utf8_to_ucs4_reflow);

	return g_test_run();
}
