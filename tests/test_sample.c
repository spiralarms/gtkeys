#include "definitions.h"
#include "sample.h"

#define HEIGHT 3
#define WIDTH 7

// not important for sample tests
#define STEPBACK 0

// helper function
gchar * get_n_words(gchar *word, guint n) {
	GString *sbuf = g_string_new("");
	for (int i = 0; i < n; i++)
		g_string_append(sbuf, word);
	return g_string_free(sbuf, FALSE);
}

gchar *word1 = "A";
gchar *word2 = "B";
guint n1 = 5;
guint n2 = 3;

gboolean generator1_flip = FALSE;

// mock
gchar * generator1(gsize width, gsize stepback, gpointer error_data) {
	generator1_flip = !generator1_flip;
	return generator1_flip == TRUE ?
			get_n_words(word1, n1) : get_n_words(word2, n2);
}

gint height_data[] = { 1, 2, 3 };

/*
 * The height of the generated texts is stored by sample module
 */
void test_sample_get_height(gconstpointer user_data) {
	gint *data = (gint*) user_data;
	// height_data size is 3
	for (int i = 0; i < 3; i++) {
		generator1_flip = FALSE;
		sample_init(data[i], WIDTH, STEPBACK, generator1);
		gint height = sample_get_height();
		sample_finalize();
		g_assert_cmpint(data[i], ==, height);
	}
}

/*
 * The utf8 length of the first line is returned.
 * The first line of all sample lines is the first line returned by the generator
 */
void test_sample_get_first_line() {
	generator1_flip = FALSE;
	sample_init(HEIGHT, WIDTH, STEPBACK, generator1);
	glong len;
	gchar *line = sample_get_first_line(&len);
	sample_finalize();
	g_assert_cmpint(len, ==, n1);
	g_assert_cmpstr(line, ==, "AAAAA");
	g_free(line);
}

/*
 * Generator returns lines without newline symbols.
 * The sample text consists of HEIGHT lines.
 * Trailing spaces are added to every line,
 * so that their length is exactly WIDTH+1 utf8 characters
 */
void test_sample_get_text() {
	generator1_flip = FALSE;
	sample_init(HEIGHT, WIDTH, STEPBACK, generator1);
	gchar *text = sample_get_text();
	sample_finalize();
	g_assert_cmpstr(text, ==, "AAAAA   \nBBB     \nAAAAA   ");
	g_free(text);
}

/*
 * Shift of the sample lines means that the first line is removed and
 * the next line is retrieved from the generator.
 *
 * sample_get_first_line must return correct first line after the shift.
 */
void test_sample_shift() {
	generator1_flip = FALSE;
	sample_init(HEIGHT, WIDTH, STEPBACK, generator1);
	sample_shift();
	gchar *text = sample_get_text();
	glong len;
	gchar *line = sample_get_first_line(&len);
	sample_finalize();
	g_assert_cmpstr(text, ==, "BBB     \nAAAAA   \nBBB     ");
	g_assert_cmpint(len, ==, n2);
	g_assert_cmpstr(line, ==, "BBB");
	g_free(text);
}

int main(int argc, char **argv) {
	g_test_init(&argc, &argv, NULL);

	g_test_add_data_func("/sample/sample_get_height", height_data,
			&test_sample_get_height);

	g_test_add_func("/sample/sample_get_first_line",
			&test_sample_get_first_line);

	g_test_add_func("/sample/sample_get_text", &test_sample_get_text);

	g_test_add_func("/sample/sample_shift", &test_sample_shift);

	return g_test_run();
}
