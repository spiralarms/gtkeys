#ifndef _GENERATORS_H_
#define _GENERATORS_H_

#include <glib.h>

gint generator_text_init(gchar *filename);
gchar * generator_text_next(gsize sl, gsize sb, gpointer error_data);

gint generator_words_init(gchar *filename);
gchar * generator_words_next(gsize width, gsize stepback, gpointer error_data);
gchar * generator_oneword_next(gsize width, gsize sb, gpointer error_data);

void generators_finalize();

void generators_set_randomize(gboolean randomize);
gunichar * generators_utf8_to_ucs4_reflow(gchar *text, glong *ucs4_size);

#endif /* _GENERATORS_H_ */
