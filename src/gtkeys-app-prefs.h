#ifndef _GTKEYS_APP_PREFS_H_
#define _GTKEYS_APP_PREFS_H_

#include <gtk/gtk.h>

#define GTKEYS_APP_PREFS_TYPE (gtkeys_app_prefs_get_type ())
#define GTKEYS_APP_PREFS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTKEYS_APP_PREFS_TYPE, GtkeysAppPrefs))

typedef struct _GtkeysAppPrefs GtkeysAppPrefs;
typedef struct _GtkeysAppPrefsClass GtkeysAppPrefsClass;

GType gtkeys_app_prefs_get_type(void);
GtkeysAppPrefs *gtkeys_app_prefs_new(GtkWindow *win);
GSettings * gtkeys_app_prefs_get_settings(GtkeysAppPrefs *prefs);
void gtkeys_app_prefs_set_default(GtkeysAppPrefs *prefs);

#endif /* _GTKEYS_APP_PREFS_H_ */
