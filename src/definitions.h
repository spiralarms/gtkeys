#ifndef _DEFINITIONS_H_
#define _DEFINITIONS_H_

#include <glib.h>
#include "config.h"

#define	WINDOW_UI  	PACKAGE_DATA_DIR"/glade/main_window.glade"
#define PREFS_UI 			"/org/spiralarms/gtkeys/prefs.ui"

// settings in the preferences dialog
#define INPUT_FONT_KEY "input-font"
#define INPUT_HIGHLIGHT_ALL_LINE_KEY 	"input-highlight-all-line"
#define AUTORESET_INTERVAL_KEY 		"autoreset-interval"

// other settings
#define TIME_SELECTOR_DEFAULT_ID_KEY 	"time-selector-default-id"
#define SAMPLE_GREY_OUT_KEY 		"sample-grey-out"
#define SAMPLE_HIGHLIGHT_CURRENT_KEY 	"sample-highlight-current"
#define SAMPLE_HIGHLIGHT_ERRORS_KEY 	"sample-highlight-errors"
#define INPUT_HIGHLIGHT_ERRORS_KEY 	"input-highlight-errors"
#define INPUT_FREEZE_CORRECT_TEXT_KEY 	"input-freeze-correct-text"
#define INPUT_KEEP_CURSOR_AT_EOL_KEY 	"input-keep-cursor-at-eol"

#define SAMPLE_HEIGHT_FOR_WORDS_KEY 	"sample-height-for-words"
#define SAMPLE_HEIGHT_FOR_TEXTS_KEY 	"sample-height-for-texts"

//#define _KEY

// for lesson and session
typedef enum {
	USER_TEXT,
	USER_WORDS,
	EN_ONEWORD,
	EN_WORDS,
	EN_TEXT,
	RU_ONEWORD,
	RU_WORDS,
	RU_TEXT,
	C_TEXT,
	CPP_TEXT,
	JAVA_TEXT,
	PYTHON_TEXT,
	BASH_TEXT
} Lesson;

// for lesson and sample
typedef gchar * (*Generator)(gsize width, gsize stepback, gpointer error_data);

// for lesson and widgets
typedef struct {
	Lesson current_lesson;
	gdouble lesson_time_fraction;
} LessonData;

// https://en.wikipedia.org/wiki/Typing#Text-entry_research
typedef struct {
	gint keystrokes; // all = C + IF + F
	gint correct_characters; // C
	gint incorrect_fixed_characters; // IF
	gint fix_keystrokes; // F, calculated
	gdouble input_time;
} Counters;

typedef struct {
	gdouble characters_per_min;
	gdouble keystrokes_per_min;
	gdouble keystrokes_per_character;
	gdouble total_error_rate;
} Metrics;

typedef struct {
	Counters counters;
	Metrics metrics;
} StatParams;

// for lesson and widgets and statistics
typedef struct {
	StatParams current_line;
	StatParams last_line;
	StatParams lesson;
} StatData;

#endif /* _DEFINITIONS_H_ */
