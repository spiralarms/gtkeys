#include "gtkeys-app.h"

// dependencies
#include "widgets.h"
#include "lesson.h"
#include "buffers.h"
#include "statistics.h"
#include "sample.h"
#include "generators.h"

struct _GtkeysAppPrivate {
	gchar dummy; // to remove a warning message
};

G_DEFINE_TYPE(GtkeysApp, gtkeys_app, GTK_TYPE_APPLICATION);

GtkeysApp * gtkeys_app_new(void) {
	return g_object_new(gtkeys_app_get_type(), "application-id",
			"org.gnome.gtkeys", "flags", G_APPLICATION_FLAGS_NONE,
			NULL);
}

static void gtkeys_app_init(GtkeysApp *gtkeys_app) {
	gtkeys_app->priv = G_TYPE_INSTANCE_GET_PRIVATE(gtkeys_app,
			GTKEYS_TYPE_APP, GtkeysAppPrivate);
}


static void gtkeys_app_finalize(GObject *object) {
	G_OBJECT_CLASS (gtkeys_app_parent_class)->finalize(object);
}

/* GApplication implementation */
static void gtkeys_app_startup(GApplication *application) {
	g_message("STARTUP");

	// Initialize Model

	G_APPLICATION_CLASS (gtkeys_app_parent_class)->startup(application);
}


static void gtkeys_app_activate(GApplication *application) {
	g_message("ACTIVATE");

	GList *list;

	// This app is single-instance
	list = gtk_application_get_windows(GTK_APPLICATION(application));
	if (list) {
		gtk_window_present(GTK_WINDOW(list->data));
		return;
	}

	//GtkeysAppPrivate *priv = GTKEYS_APP(application)->priv;

	// Initialize View and Presenter
	widgets_init(application);
}

static void gtkeys_app_shutdown(GApplication *application) {
	g_message("SHUTDOWN");

	widgets_finalize();

	G_APPLICATION_CLASS (gtkeys_app_parent_class)->shutdown(application);
}

static void gtkeys_app_class_init(GtkeysAppClass *klass) {
	GObjectClass* object_class = G_OBJECT_CLASS(klass);

	g_type_class_add_private(klass, sizeof(GtkeysAppPrivate));

	G_APPLICATION_CLASS (klass)->startup = gtkeys_app_startup;
	G_APPLICATION_CLASS (klass)->activate = gtkeys_app_activate;
	G_APPLICATION_CLASS (klass)->shutdown = gtkeys_app_shutdown;

	object_class->finalize = gtkeys_app_finalize;
}
