#include "statistics.h"

StatData sd;

static const StatParams sp_zero;

/*
 * Constructor
 */
void statistics_init() {
	//g_message("statistics_init");

	statistics_reset_lesson_data(); // initial state
}

/*
 * Add a keystroke
 */
void statistics_add_keystroke() {
	sd.current_line.counters.keystrokes++;
}

/*
 * Add a correct character
 */
void statistics_add_correct_character(gunichar character, gdouble line_time) {
	sd.current_line.counters.correct_characters++;
}

/*
 * Add an incorrect character (all of them are fixed)
 */
void statistics_add_incorrect_fixed_character(gunichar character, gdouble line_time) {
	sd.current_line.counters.incorrect_fixed_characters++;
}

void statistics_reset_line_data() {
	//g_message("statistics_reset_line_data");
	sd.current_line = sp_zero;
}

/*
 * Add statistical data for the line
 */
void statistics_append_line_data(gdouble line_time) {
	//g_message("statistics_append_line_data");

	// Order matters
	sd.last_line.counters = sd.current_line.counters;
	sd.last_line.counters.input_time = line_time;

	sd.lesson.counters.keystrokes +=
			sd.current_line.counters.keystrokes;
	sd.lesson.counters.correct_characters +=
			sd.current_line.counters.correct_characters;
	sd.lesson.counters.incorrect_fixed_characters +=
			sd.current_line.counters.incorrect_fixed_characters;
	sd.lesson.counters.fix_keystrokes +=
			sd.current_line.counters.fix_keystrokes;
	sd.lesson.counters.input_time += line_time;

	statistics_reset_line_data();
}

/*
 * Reset lesson data
 */
void statistics_reset_lesson_data() {
	//g_message("statistics_reset_lesson_data");

	statistics_reset_line_data();
	sd.last_line = sp_zero;
	sd.lesson = sp_zero;
}

/*
 * Helper function
 */
static gdouble division(gdouble dividend, gdouble divisor) {
	return divisor > 0.0 ? dividend/divisor : 0.0;
}

/*
 * Calculate metrics from counters
 */
static void statistics_calculate_metrics(StatParams *sp) {

	sp->counters.fix_keystrokes = sp->counters.keystrokes
			- sp->counters.correct_characters
			- sp->counters.incorrect_fixed_characters;

	sp->metrics.characters_per_min = division(60.0 * sp->counters.correct_characters,
			sp->counters.input_time);

	sp->metrics.total_error_rate = division(100.0 * sp->counters.incorrect_fixed_characters,
			sp->counters.correct_characters + sp->counters.incorrect_fixed_characters);

	sp->metrics.keystrokes_per_min = division(60.0 * sp->counters.keystrokes,
			sp->counters.input_time);

	sp->metrics.keystrokes_per_character = division(sp->counters.keystrokes,
			sp->counters.correct_characters);
}

/*
 * Calculate metrics from counters for last line data and lesson data
 */
StatData * statistics_get_lesson_data() {

	statistics_calculate_metrics(&sd.last_line);
	statistics_calculate_metrics(&sd.lesson);

	return &sd;
}

/*
 * Destructor
 */
void statistics_finalize() {
	//g_message("statistics_finalize");
}
