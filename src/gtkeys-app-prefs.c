#include <gtk/gtk.h>

#include "gtkeys-app-prefs.h"
#include "definitions.h"

struct _GtkeysAppPrefs {
	GtkDialog parent;
};

struct _GtkeysAppPrefsClass {
	GtkDialogClass parent_class;
};

typedef struct _GtkeysAppPrefsPrivate GtkeysAppPrefsPrivate;

struct _GtkeysAppPrefsPrivate {
	GSettings *settings;
	GtkWidget *fb_font;
	GtkWidget *sb_autoreset_interval;

	GtkWidget *cb_input_highlight_all_line;
	GtkWidget *cb_sample_grey_out;
	GtkWidget *cb_sample_highlight_current;
	GtkWidget *cb_sample_highlight_errors;
	GtkWidget *cb_input_highlight_errors;
	GtkWidget *cb_input_freeze_correct_text;
	GtkWidget *cb_input_keep_cursor_at_eol;
};

G_DEFINE_TYPE_WITH_PRIVATE(GtkeysAppPrefs, gtkeys_app_prefs, GTK_TYPE_DIALOG)

/*
 * Widgets in Preferences dialog are directly bound to the corresponding settings
 */
static void gtkeys_app_prefs_init(GtkeysAppPrefs *prefs) {
	GtkeysAppPrefsPrivate *priv;

	priv = gtkeys_app_prefs_get_instance_private(prefs);
	gtk_widget_init_template(GTK_WIDGET(prefs));
	priv->settings = g_settings_new("org.spiralarms.gtkeys");

	// Binding

	g_settings_bind(priv->settings, INPUT_FONT_KEY, priv->fb_font, "font",
			G_SETTINGS_BIND_DEFAULT);

	// Double value of the adjustment is automatically converted to integer value in GSettings
	g_settings_bind(priv->settings, AUTORESET_INTERVAL_KEY,
			gtk_spin_button_get_adjustment(
				GTK_SPIN_BUTTON(priv->sb_autoreset_interval)),
			"value", G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, INPUT_HIGHLIGHT_ALL_LINE_KEY,
			GTK_CHECK_BUTTON(priv->cb_input_highlight_all_line), "active",
			G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, SAMPLE_GREY_OUT_KEY,
			GTK_CHECK_BUTTON(priv->cb_sample_grey_out), "active",
			G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, SAMPLE_HIGHLIGHT_CURRENT_KEY,
			GTK_CHECK_BUTTON(priv->cb_sample_highlight_current), "active",
			G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, SAMPLE_HIGHLIGHT_ERRORS_KEY,
			GTK_CHECK_BUTTON(priv->cb_sample_highlight_errors), "active",
			G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, INPUT_HIGHLIGHT_ERRORS_KEY,
			GTK_CHECK_BUTTON(priv->cb_input_highlight_errors), "active",
			G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, INPUT_FREEZE_CORRECT_TEXT_KEY,
			GTK_CHECK_BUTTON(priv->cb_input_freeze_correct_text), "active",
			G_SETTINGS_BIND_DEFAULT);

	g_settings_bind(priv->settings, INPUT_KEEP_CURSOR_AT_EOL_KEY,
			GTK_CHECK_BUTTON(priv->cb_input_keep_cursor_at_eol), "active",
			G_SETTINGS_BIND_DEFAULT);

}

static void gtkeys_app_prefs_dispose(GObject *object) {
	GtkeysAppPrefsPrivate *priv;

	priv = gtkeys_app_prefs_get_instance_private(GTKEYS_APP_PREFS(object));
	g_clear_object(&priv->settings);

	G_OBJECT_CLASS (gtkeys_app_prefs_parent_class)->dispose(object);
}

static void gtkeys_app_prefs_class_init(GtkeysAppPrefsClass *class) {
	G_OBJECT_CLASS (class)->dispose = gtkeys_app_prefs_dispose;

	gtk_widget_class_set_template_from_resource(GTK_WIDGET_CLASS(class),
			PREFS_UI);

	// Binding template widgets to private pointers!

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, fb_font);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, sb_autoreset_interval);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_input_highlight_all_line);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_sample_grey_out);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_sample_highlight_current);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_sample_highlight_errors);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_input_highlight_errors);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_input_freeze_correct_text);

	gtk_widget_class_bind_template_child_private(GTK_WIDGET_CLASS (class),
			GtkeysAppPrefs, cb_input_keep_cursor_at_eol);
}

GtkeysAppPrefs * gtkeys_app_prefs_new(GtkWindow *win) {
	return g_object_new(GTKEYS_APP_PREFS_TYPE, NULL);
}

GSettings * gtkeys_app_prefs_get_settings(GtkeysAppPrefs *prefs) {
	GtkeysAppPrefsPrivate *priv;
	priv = gtkeys_app_prefs_get_instance_private(prefs);
	return priv->settings;
}

static void set_default(GSettings *settings, const gchar *key) {
	GVariant *var = g_settings_get_default_value(settings, key);
	g_settings_set_value(settings, key, var);
	g_object_unref(var);
}

void gtkeys_app_prefs_set_default(GtkeysAppPrefs *prefs) {
	GtkeysAppPrefsPrivate *priv;
	priv = gtkeys_app_prefs_get_instance_private(prefs);

	set_default(priv->settings, INPUT_FONT_KEY);
	set_default(priv->settings, AUTORESET_INTERVAL_KEY);
	set_default(priv->settings, INPUT_HIGHLIGHT_ALL_LINE_KEY);
	set_default(priv->settings, SAMPLE_GREY_OUT_KEY);
	set_default(priv->settings, SAMPLE_HIGHLIGHT_CURRENT_KEY);
	set_default(priv->settings, SAMPLE_HIGHLIGHT_ERRORS_KEY);
	set_default(priv->settings, INPUT_HIGHLIGHT_ERRORS_KEY);
	set_default(priv->settings, INPUT_FREEZE_CORRECT_TEXT_KEY);
	set_default(priv->settings, INPUT_KEEP_CURSOR_AT_EOL_KEY);
}
