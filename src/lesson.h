#ifndef _LESSON_H_
#define _LESSON_H_

#include <glib.h>
#include <gtk/gtk.h>

// dependencies
#include "definitions.h"

#define	 EN_WORDS_FILE			PACKAGE_DATA_DIR"/data/en_words.txt"
#define	 EN_SYMBOLS_FILE		PACKAGE_DATA_DIR"/data/en_symbols.txt"
#define	 EN_TEXT_FILE			PACKAGE_DATA_DIR"/data/en_text.txt"
#define	 RU_WORDS_FILE			PACKAGE_DATA_DIR"/data/ru_words.txt"
#define	 RU_SYMBOLS_FILE		PACKAGE_DATA_DIR"/data/ru_symbols.txt"
#define	 RU_TEXT_FILE			PACKAGE_DATA_DIR"/data/ru_text.txt"
#define	 NUMBERS_FILE			PACKAGE_DATA_DIR"/data/numbers.txt"
#define	 C_TEXT_FILE			PACKAGE_DATA_DIR"/data/c_text.txt"
#define	 CPP_TEXT_FILE			PACKAGE_DATA_DIR"/data/cpp_text.txt"
#define	 JAVA_TEXT_FILE			PACKAGE_DATA_DIR"/data/java_text.txt"
#define	 PYTHON_TEXT_FILE		PACKAGE_DATA_DIR"/data/python_text.txt"
#define	 BASH_TEXT_FILE			PACKAGE_DATA_DIR"/data/bash_text.txt"

#define	 SHORT_STRING_LENGTH	40
#define	 STRING_LENGTH		60

#define	 MIN_STEPBACK		1
#define	 STEPBACK		10

void lesson_init(GSettings *gs);
void lesson_finalize();

void lesson_refresh();

void lesson_autobackspace(gboolean autobackspace);

void lesson_set_randomize(gboolean randomize);

void lesson_input_buffer_insert_text(gint location_offset, gchar *text);
void lesson_input_buffer_changed();
void lesson_sample_buffer_changed();
void lesson_select(Lesson lesson, gchar *filename);

void lesson_set_time(gint lesson_time_min);

gboolean lesson_key_press_event(GdkEventKey *pKey);

void lesson_show_preferences();

#endif /* _LESSON_H_ */
