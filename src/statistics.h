#ifndef _STATISTICS_H_
#define _STATISTICS_H_

#include <glib.h>

// dependencies
#include "definitions.h"


void statistics_add_keystroke();
void statistics_add_correct_character(gunichar character, gdouble line_time);
void statistics_add_incorrect_fixed_character(gunichar character, gdouble line_time);

void statistics_init();
void statistics_finalize();

void statistics_reset_line_data();
void statistics_append_line_data(gdouble line_time);

void statistics_reset_lesson_data();

StatData * statistics_get_lesson_data();

#endif /* _STATISTICS_H_ */
