#include <gio/gio.h>
#include "generators.h"
#include "definitions.h"

#define UCS4_MAX_COUNT 500
#define UCS4_WORD_SEARCH_RANGE 40

gunichar *ucs4 = NULL; // ucs4 buffer
glong ucs4_index; // current symbol
glong ucs4_size; // the size of the buffer
gint ucs4_count; // symbol counter
gboolean randomize_text = TRUE; // text randomization flag

GPtrArray *words = NULL; // words from the dictionary

/*
 * Set the flag of text randomization.
 * Affects the behavior of text generator.
 * If TRUE, the position in text randomly changes every several lines.
 */
void generators_set_randomize(gboolean randomize) {
	randomize_text = randomize;
}

/*
 * Convert utf8 text to ucs buffer and replace multiple spaces and line ends with one space.
 * For example, the text "   word1  word2 word3" transforms to "word1 word2 word3 ".
 * First symbol is non-space, and there is a space at the end.
 *
 * @text (in) utf8 text
 * @ucs4_size (out) the size of the buffer
 * @return ucs4 buffer, without '\0' at the end. Must be freed with g_free().
 */
gunichar * generators_utf8_to_ucs4_reflow(gchar *text, glong *ucs4_size) {
	gchar *text1;
	gunichar *ucs4, *p, *q;

	if (text == NULL)
		return NULL;

	// Convert to ucs, with normalization

	if (g_utf8_validate (text, -1, NULL) == FALSE)
		return NULL;

	text1 = g_utf8_normalize(text, -1,  G_NORMALIZE_ALL_COMPOSE);

	ucs4 = g_utf8_to_ucs4_fast(text1, -1, NULL);
	if (ucs4 == NULL)
		return NULL;

	g_free(text1);

	// Replace multiple spaces and newlines with one space
	p = q = ucs4;
	gboolean space = TRUE; // spaces are skipped, so resulting ucs4 buffer is empty or starts with non-space
	// One word must correspond to one space. 
	// Oddity marks a difference between their quantities
	gboolean oddity = FALSE;
	while (*p) {
		if (!g_unichar_isspace(*p)) {
			*q++ = *p;
			if (space == TRUE)
				oddity = TRUE; // a word is found
			space = FALSE;
		} else if (space == FALSE) {
			*q++ = 0x20;
			oddity = FALSE; // a space is found
			space = TRUE;
		}
		p++;
	}

	if (oddity) // the number of words is larger than the number of spaces
		*q++ = 0x20; // add a space

	*ucs4_size = q - ucs4; // calculate the size of the buffer

	return ucs4;
}

/*
 * Set random position in the ucs4 text buffer
 * and try to find the beginning of a word by searching forward
 * in a ring-like structure of the buffer.
 */
static void generator_text_set_random_position() {
	// Set random position (last value excluded)
	ucs4_index = g_random_int_range(0, ucs4_size);

	// Try to find the beginning of the word (moving forward)
	gint i;
	gint wsr = MIN(ucs4_size - 1, UCS4_WORD_SEARCH_RANGE);

	for (i = 0; i < wsr; i++) {
		// For d=0 this is the value before fields.ucs4_index in a ring-like structure 
		glong index1 = (ucs4_size + ucs4_index + i - 1)
				% ucs4_size;
		if (g_unichar_isspace(ucs4[index1])) {
			// For d=0 this value equals fields.ucs4_index
			ucs4_index = (ucs4_index + i)
					% ucs4_size;
			break;
		}
	}

	ucs4_count = 0;
}

/*
 * Destructor the generator of text
 */
static void generators_text_finalize() {
	//g_message("generators_text_finalize");

	g_free(ucs4);
}

/*
 * Load the text and set random starting position
 *
 * @filename The name of text (correct UTF8) file.
 * @return 0 for success, <0 for an error
 */
gint generator_text_init(gchar *filename) {
	gchar *text;

	gboolean status = g_file_get_contents(filename, &text, NULL, NULL);
	if (status == FALSE)
		return -2;

	// Convert to ucs4 and remove multiple spaces
	generators_text_finalize();
	ucs4 = generators_utf8_to_ucs4_reflow(text, &ucs4_size);
	g_free(text);

	if (ucs4 == NULL) return -3;

	if (randomize_text == TRUE)
		generator_text_set_random_position();

	return 0;
}

/*
 * Generate the next line of text with better line ending.
 * It should be freed by g_free()
 *
 * @width The maximal utf8 length of generated text lines.
 * @stepback The maximal offset to step back when looking for a word separator.
 * @error_data (unused)
 */
gchar * generator_text_next(gsize width, gsize stepback, gpointer error_data) {
	gchar *line;

	g_assert(width >= stepback);

	/*if (sl > fields.ucs4_size) sl = fields.ucs4_size - 1;
	 if (sl < 0) sl = 0;
	 if (sb > sl) sb = sl;
	 */

	// Start index
	glong index1 = ucs4_index;

	if (g_unichar_isspace(ucs4[index1]))
		index1 = (index1 + 1) % ucs4_size; // skip a space

	// End index
	glong index2 = (index1 + width) % ucs4_size;

	// Try to form a nicer line ending
	if (!g_unichar_isspace(ucs4[index2])) {

		// Not a space, it may be the middle of the word.
		// Try to step back from index2 and find a space.
		for (glong i = 0; i < stepback; i++) {
			// For i=0 this is the value before fields.ucs4_index in a ring-like structure 
			glong i2 = (ucs4_size + index2 - i - 1)
					% ucs4_size;
			if (g_unichar_isspace(ucs4[i2])) {
				index2 = i2;
				break;
			}
		}
	}

	// Form a line from symbols between indices index1 and index2.
	// This is the ring-like structure, so indices may be in reversed order.
	if (index1 >= index2) {
		gchar *line1 = g_ucs4_to_utf8(ucs4 + index1,
				ucs4_size - index1, NULL, NULL, NULL);
		gchar *line2 = g_ucs4_to_utf8(ucs4, index2, NULL, NULL,
		NULL);
		line = g_strconcat(line1, line2, NULL);
		g_free(line1);
		g_free(line2);
	} else {
		line = g_ucs4_to_utf8(ucs4 + index1, index2 - index1,
		NULL, NULL, NULL);
	}

	// Current position.
	ucs4_index = index2;

	// Jump to random position after several lines of text
	ucs4_count += g_utf8_strlen(line, -1);

	if (randomize_text == TRUE && ucs4_count > UCS4_MAX_COUNT)
		generator_text_set_random_position();

	g_assert(g_utf8_strlen(line, -1) <= width);

	return line;
}

/*
 * Destructor the generator of words
 */
static void generators_words_finalize() {
	//g_message("generators_words_finalize");

	if (words != NULL) {
		for (int i = 0; i < words->len; i++)
			g_free(g_ptr_array_index(words, i));
		g_ptr_array_free(words, TRUE);
	}

	words = NULL;
}

/*
 * Load the dictionary
 *
 * @filename The name of text (correct UTF8) file
 * containing a word at every line.
 *
 * @return 0 for success, <0 for an error
 */
gint generator_words_init(gchar *filename) {
	gchar *line;
	gsize length;
	GError *error = NULL;

	GFile *file = g_file_new_for_path(filename);

	// Streams are closed automatically
	GInputStream *stream = G_INPUT_STREAM(g_file_read (file, NULL, &error));
	if (!stream && error) {
		g_object_unref(file);
		return -1;
	}

	GDataInputStream *datain = g_data_input_stream_new(stream);

	// destroy old words
	generators_words_finalize();

	// create new words
	words = g_ptr_array_new();
	while ((line = g_data_input_stream_read_line_utf8(datain, &length, NULL,
			&error)) != NULL) {
		g_ptr_array_add(words, (gpointer) line);
	}

	g_object_unref(file);
	g_object_unref(stream);
	g_object_unref(datain);
	return 0;
}

/*
 * Generate the line of a random word.
 * It must be freed with g_free()
 *
 * @width The maximal utf8 length of generated text lines.
 * @stepback (unused)
 * @error_data (unused)
 *
 */
gchar *
generator_oneword_next(gsize width, gsize stepback, gpointer error_data) {

	gint index = g_random_int_range(0, words->len); // last excluded
	gchar *word = g_ptr_array_index(words, index);

	GString *s = g_string_new("");
	glong len;
	while ((len = g_utf8_strlen(s->str, -1)) + g_utf8_strlen(word, -1) + 1 <= width) {
		if (len > 0)
			g_string_append(s, " ");
		g_string_append(s, word);

	}
	gchar *line = g_string_free(s, FALSE);

	g_assert(g_utf8_strlen(line, -1) <= width);

	return line;
}

/*
 * Generate the line of random words
 * It must be freed with g_free()
 *
 * @width The maximal utf8 length of generated text lines.
 * @stepback (unused)
 * @error_data (unused)
 */
gchar * generator_words_next(gsize width, gsize stepback, gpointer error_data) {

	GString *s = g_string_new("");
	for (;;) {
		gint index = g_random_int_range(0, words->len); // last excluded
		gchar *word = g_ptr_array_index(words, index);
		glong len = g_utf8_strlen(s->str, -1);
		if (len + 1 + g_utf8_strlen(word, -1)
				> width)
			break;
		if (len > 0)
			g_string_append(s, " ");
		s = g_string_append(s, word);
	}
	gchar *line = g_string_free(s, FALSE);

	g_assert(g_utf8_strlen(line, -1) <= width);

	return line;
}

/*
 * Destructor
 */
void generators_finalize() {
	//g_message("generators_finalize");

	generators_text_finalize();
	generators_words_finalize();

}
