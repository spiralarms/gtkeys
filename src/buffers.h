#ifndef _BUFFERS_H_
#define _BUFFERS_H_

#include <gtk/gtk.h>

void buffers_init(GtkTextBuffer *input, GtkTextBuffer *sample);
void buffers_finalize();

void buffers_input_clear();
gchar * buffers_input_get_first_line();
void buffers_input_set_tags(gint offset, gboolean warning);
void buffers_input_insert_at_cursor(gchar *text);

void buffers_sample_set_text(gchar *text);
void buffers_sample_set_tags(gint offset, gboolean warning);

gboolean buffers_do_autobackspace(gpointer offset);

void buffers_set_parameters(
		gboolean sample_grey_out,
		gboolean sample_highlight_current,
		gboolean sample_highlight_errors,
		gboolean input_highlight_errors,
		gboolean input_highlight_all_line,
		gboolean input_freeze_correct_text,
		gboolean input_keep_cursor_at_eol);

#endif /* _BUFFERS_H_ */
