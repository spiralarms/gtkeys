#ifndef SRC_WIDGETS_H_
#define SRC_WIDGETS_H_

#include <gtk/gtk.h>

// dependencies
#include "definitions.h"

// for application
void widgets_init();
void widgets_finalize();

// for buffers
GtkTextBuffer * widgets_get_input_buffer();
GtkTextBuffer * widgets_get_sample_buffer();

// for lesson
void widgets_textviews_set_sensitive(gboolean sensitive);
void widgets_apply_font(gchar *fontname);
void widgets_show_preferences();
void widgets_update_statdata(LessonData *ld, StatData *sd);

#endif /* SRC_WIDGETS_H_ */
