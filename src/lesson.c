#include "config.h"
#include "lesson.h"

// dependencies
#include "widgets.h"
#include "buffers.h"
#include "sample.h"
#include "generators.h"
#include "statistics.h"

#define AUTOBACKSPACE_INTERVAL 200

typedef enum {
	IDLE,
	RUNNING,
	WAITING
} LessonState;

LessonState lesson_state;

GSettings *settings;

gunichar *ucs4_input = NULL;
gunichar *ucs4_sample = NULL;

glong len_input, len_sample;
gint current_offset;
gunichar current_character;

GTimer *line_timer;
GTimer *idle_timer;

LessonData lesson_data;

guint lesson_time; // time selector
gboolean use_autobackspace = FALSE; // autobackspace button

glong autoreset_interval_id = -1;
guint autoreset_interval; // preferences

guint sample_height_for_words; // settings
guint sample_height_for_texts; // settings

gboolean skip_lesson_run = FALSE; // helper flag

/*
 * Update statistical data widgets and the time progress bar
 * TODO: are there other widgets involved?
 */
static void lesson_update_statistics() {
	//g_message("lesson_update_statistics");

	StatData *sd = statistics_get_lesson_data();

	// Prepare lesson data
	lesson_data.lesson_time_fraction = sd->lesson.counters.input_time / lesson_time;

	widgets_update_statdata(&lesson_data, sd);
}

/*
 * Set lesson state
 */
static void set_state(LessonState state) {
	lesson_state = state;
}


/*
 * Reset line
 *
 * RUNNING -> WAITING
 */
static void lesson_reset() {
	//g_message("lesson_reset");

	if (lesson_state == RUNNING) {
		statistics_reset_line_data();
		//update_statistics();

		// buffers before timers
		skip_lesson_run = TRUE;
		buffers_input_clear();

		g_timer_stop(line_timer);
		g_timer_reset(line_timer);
		g_timer_stop(idle_timer);
		g_timer_reset(idle_timer);

		set_state(WAITING);
	}
}

/*
 * Change lesson state to RUNNING when the input buffer changes.
 *
 * IDLE -> RUNNING
 * WAITING -> RUNNING
 */
static void lesson_run() {
	//g_message("lesson_run");

	if (lesson_state == IDLE || lesson_state == WAITING) {

		if (lesson_state == IDLE) {
			statistics_reset_lesson_data();
			lesson_update_statistics();
			//widgets_tbtn_stop_enable(TRUE);
		}

		g_timer_start(line_timer);

		set_state(RUNNING);
	}

	g_timer_start(idle_timer);
}

/*
 * Autoreset input line
 */
static gboolean lesson_autoreset_input_line(gpointer user_data) {
	// seconds
	gdouble idle_time = g_timer_elapsed(idle_timer, NULL);

	if (idle_time > (gdouble)autoreset_interval)
		lesson_reset();

	return G_SOURCE_CONTINUE;
}

/*
 * Apply settings to widgets and parameters
 */
static void lesson_apply_settings() {
	//g_message("lesson_apply_settings");

	// Input font
	gchar *fontname = g_settings_get_string(settings, INPUT_FONT_KEY);
	widgets_apply_font(fontname);
	g_free(fontname);

	// Autoreset
	autoreset_interval = g_settings_get_uint(settings,
			AUTORESET_INTERVAL_KEY);
	if (autoreset_interval_id >= 0)
		g_source_remove(autoreset_interval_id);
	autoreset_interval_id = g_timeout_add(autoreset_interval,
			lesson_autoreset_input_line, NULL);

	// Parameters of text buffers
	gboolean sample_grey_out = g_settings_get_boolean(settings,
			SAMPLE_GREY_OUT_KEY);
	gboolean sample_highlight_current = g_settings_get_boolean(settings,
			SAMPLE_HIGHLIGHT_CURRENT_KEY);
	gboolean sample_highlight_errors = g_settings_get_boolean(settings,
			SAMPLE_HIGHLIGHT_ERRORS_KEY);
	gboolean input_highlight_errors = g_settings_get_boolean(settings,
			INPUT_HIGHLIGHT_ERRORS_KEY);
	gboolean input_highlight_all_line = g_settings_get_boolean(settings,
			INPUT_HIGHLIGHT_ALL_LINE_KEY);

	gboolean input_freeze_correct_text = g_settings_get_boolean(settings,
				INPUT_FREEZE_CORRECT_TEXT_KEY);
	gboolean input_keep_cursor_at_eol = g_settings_get_boolean(settings,
				INPUT_KEEP_CURSOR_AT_EOL_KEY);

	buffers_set_parameters(
			sample_grey_out,
			sample_highlight_current,
			sample_highlight_errors,
			input_highlight_errors,
			input_highlight_all_line,
			input_freeze_correct_text,
			input_keep_cursor_at_eol);

	// The height of sample textview
	sample_height_for_words = g_settings_get_uint(settings,
			SAMPLE_HEIGHT_FOR_WORDS_KEY);
	sample_height_for_texts = g_settings_get_uint(settings,
			SAMPLE_HEIGHT_FOR_TEXTS_KEY);
}

void lesson_show_preferences() {
	lesson_reset();
	widgets_show_preferences();
	lesson_apply_settings();
}

void lesson_autobackspace(gboolean autobackspace) {
	use_autobackspace = autobackspace;
}



/*
 * Handler of key presses in the input textview.
 * See the <gdk/gdkkeysyms.h> header file
 * for a complete list of GDK key codes.
 *
 * Send keystrokes to statistics.
 * Redefine the behavior of the Return key.
 */
gboolean lesson_key_press_event(GdkEventKey *pKey) {

	if (pKey->type == GDK_KEY_PRESS) {

		statistics_add_keystroke();

		if (pKey->keyval == GDK_KEY_Return) {
			buffers_input_insert_at_cursor("¶");
			return TRUE;
		}
	}

	return FALSE;
}

/*
 * Handler of randomize toggle tool button
 */
void lesson_set_randomize(gboolean randomize) {
	generators_set_randomize(randomize);
}

/*
 * It should be done after every buffer change (input or sample)
 * Convert
 */
static void lesson_update_global_state() {
	//g_message("lesson_update_global_state");

	g_free(ucs4_input);
	g_free(ucs4_sample);

	gchar *line = NULL;

	line = buffers_input_get_first_line();
	ucs4_input = g_utf8_to_ucs4_fast(line, -1, &len_input);
	g_free(line);

	line = sample_get_first_line(&len_sample);
	ucs4_sample = g_utf8_to_ucs4_fast(line, -1, NULL);
	g_free(line);

	// Calculate current_offset - the offset of the first different
	// character from the beginning of input line and first sample line
	// This is the position of the next correct input character
	current_offset = 0;
	for (; current_offset < MIN(len_input, len_sample); current_offset++)
		if (ucs4_input[current_offset] != ucs4_sample[current_offset])
			break;

	// next correct input character
	current_character = len_sample > 0 ? ucs4_sample[current_offset] : -1;
}

/*
 * Handle transition to the next sample line
 *
 * RUNNING -> WAITING
 */
static void lesson_new_line() {
	//g_message("lesson_new_line");

	gdouble line_time = g_timer_elapsed(line_timer, NULL);
	statistics_append_line_data(line_time);

	lesson_update_statistics();

	lesson_reset(); // RUNNING -> WAITING

	sample_shift();

	gchar *text = sample_get_text();
	buffers_sample_set_text(text);
	g_free(text);
}

/*
 * Handler of the input buffer change.
 *
 * The call to lesson_run may be skipped,
 * if skip_lesson_run == TRUE.
 */
void lesson_input_buffer_changed() {
	//g_message("lesson_input_buffer_changed");

	if (skip_lesson_run == TRUE)
		skip_lesson_run = FALSE;
	else
		lesson_run();

	lesson_update_global_state();

	// Check conditions of moving to the next sample line.
	// GDK_KEY_paragraph corresponds to '¶'.
	if (current_offset == len_sample &&
		current_offset < len_input &&
		(ucs4_input[current_offset] == GDK_KEY_paragraph
			|| g_unichar_isspace(ucs4_input[current_offset]))) {

		skip_lesson_run = TRUE;
		lesson_new_line();
		return;
	}

	gboolean warning = current_offset < len_input;

	if (warning && use_autobackspace)
		g_timeout_add(AUTOBACKSPACE_INTERVAL, buffers_do_autobackspace, &current_offset);

	buffers_input_set_tags(current_offset, warning);
	buffers_sample_set_tags(current_offset, warning);
}

/*
 * Handler of the sample buffer change.
 */
void lesson_sample_buffer_changed() {
	//g_message("lesson_sample_buffer_changed");

	lesson_update_global_state();
	buffers_sample_set_tags(0, FALSE);
}







/*
 * Refresh sample text
 *
 * RUNNING -> WAITING
 */
void lesson_refresh() {
	//g_message("lesson_refresh");

	lesson_reset();

	for (int i = 0; i < sample_get_height(); i++)
		sample_shift();

	gchar *text = sample_get_text();
	buffers_sample_set_text(text);
	g_free(text);
}

/*
 * Stop current lesson before starting the new one.
 * Reset statistical data.
 *
 * RUNNING -> IDLE
 * WAITING -> IDLE
 */
static void lesson_stop() {
	//g_message("lesson_stop");

	lesson_reset();

	if (lesson_state == WAITING) {
		statistics_reset_lesson_data();
		lesson_update_statistics();
			
		//widgets_tbtn_stop_enable(FALSE);

		set_state(IDLE);
	}
}

/*
 * Select the type of lesson
 *
 * RUNNING -> IDLE
 * WAITING -> IDLE
 */
void lesson_select(Lesson lesson, gchar *filename) {
	//g_message("lesson_select");

	lesson_data.current_lesson = lesson;

	gint status = -1;

	lesson_stop();

	switch (lesson) {

	case EN_ONEWORD:
		status = generator_words_init(EN_WORDS_FILE);
		if (status == 0)
			sample_init(sample_height_for_words, STRING_LENGTH, STEPBACK,
					generator_oneword_next);
		break;

	case RU_ONEWORD:
		status = generator_words_init(RU_WORDS_FILE);
		if (status == 0)
			sample_init(sample_height_for_words, STRING_LENGTH, STEPBACK,
					generator_oneword_next);
		break;

	case EN_WORDS:
		status = generator_words_init(EN_WORDS_FILE);
		if (status == 0)
			sample_init(sample_height_for_words, STRING_LENGTH, STEPBACK,
					generator_words_next);
		break;

	case RU_WORDS:
		status = generator_words_init(RU_WORDS_FILE);
		if (status == 0)
			sample_init(sample_height_for_words, STRING_LENGTH, STEPBACK,
					generator_words_next);
		break;

	case EN_TEXT:
		status = generator_text_init(EN_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case RU_TEXT:
		status = generator_text_init(RU_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case C_TEXT:
		status = generator_text_init(C_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case CPP_TEXT:
		status = generator_text_init(CPP_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case JAVA_TEXT:
		status = generator_text_init(JAVA_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case PYTHON_TEXT:
		status = generator_text_init(PYTHON_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case BASH_TEXT:
		status = generator_text_init(BASH_TEXT_FILE);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case USER_TEXT:
		status = generator_text_init(filename);
		if (status == 0)
			sample_init(sample_height_for_texts, STRING_LENGTH, STEPBACK,
					generator_text_next);
		break;

	case USER_WORDS:
		status = generator_words_init(filename);
		if (status == 0)
			sample_init(sample_height_for_words, STRING_LENGTH, STEPBACK,
					generator_words_next);
		break;

	default:
		;

	}

	if (status != 0) {
		g_message("Error when loading text file: %d", status);
		buffers_input_clear();
		buffers_sample_set_text("");
		widgets_textviews_set_sensitive(FALSE);
	} else {
		gchar *text = sample_get_text();
		buffers_sample_set_text(text);
		g_free(text);
		widgets_textviews_set_sensitive(TRUE);
	}

	lesson_update_statistics();
}



/*
 * Constructor
 */
void lesson_init(GSettings *gs) {
	//g_message("lesson_init");

	settings = gs;

	statistics_init();

	GtkTextBuffer *input = widgets_get_input_buffer();
	GtkTextBuffer *sample = widgets_get_sample_buffer();
	buffers_init(input, sample);

	//widgets_tbtn_stop_enable(FALSE);

	line_timer = g_timer_new ();
	idle_timer = g_timer_new ();
	g_timer_stop(line_timer);
	g_timer_stop(idle_timer);

	set_state(IDLE);

	lesson_apply_settings();

	lesson_select(EN_ONEWORD, NULL);
}

/*
 * Destructor
 */
void lesson_finalize() {
	//g_message("lesson_finalize");

	if (autoreset_interval_id >= 0)
		g_source_remove(autoreset_interval_id);

	g_timer_destroy(line_timer);
	g_timer_destroy(idle_timer);

	g_free(ucs4_input);
	g_free(ucs4_sample);

	buffers_finalize();

	statistics_finalize();
	sample_finalize();
	generators_finalize();
}

/*
 * Add input characters to statistical data
 */
void lesson_input_buffer_insert_text(gint location_offset, gchar *text) {
	//g_message("lesson_input_buffer_insert_text");

	long len_ucs4;
	gunichar *ucs4 = g_utf8_to_ucs4_fast(text, -1, &len_ucs4);

	gdouble line_time = g_timer_elapsed(line_timer, NULL);

	gboolean flag = FALSE;

	if (location_offset == current_offset)
		flag = TRUE;

	for (int i = 0; i < len_ucs4; i++) {
		glong offset = location_offset + i;
		if (flag && offset < len_sample
				&& ucs4[i] == ucs4_sample[offset]) {
			statistics_add_correct_character(ucs4[i], line_time);
		} else {
			// Space or enter key
			if (flag && offset == len_sample
					&& (ucs4[i] == GDK_KEY_paragraph
							|| g_unichar_isspace(ucs4[i]))) {
				statistics_add_correct_character(GDK_KEY_paragraph, line_time);
			} else {
				statistics_add_incorrect_fixed_character(ucs4[i], line_time);
			}
			// Characters following the last correct
			// or the first incorrect character are incorrect too.
			flag = FALSE;
		}
	}

	g_free(ucs4);
}

/*
 * Set time of the lesson. It is used only for time progress bar.
 */
void lesson_set_time(gint lesson_time_min) {
	//g_message("lesson_set_time");

	lesson_time = 60 * lesson_time_min;
	lesson_update_statistics();
}
