#include <string.h>
#include "buffers.h"

// dependencies
#include "lesson.h"

#define INPUT_READONLY "readonly"
#define INPUT_WARNING_COLOR "input_warning_color"
#define SAMPLE_READY_COLOR "sample_ready_color"
#define SAMPLE_CURRENT_COLOR "sample_current_color"
#define SAMPLE_WARNING_COLOR "sample_warning_color"

GtkTextBuffer *tb_input;
GtkTextBuffer *tb_sample;

gboolean sample_grey_out_;
gboolean sample_highlight_current_;
gboolean sample_highlight_errors_;
gboolean input_highlight_errors_;
gboolean input_highlight_all_line_;
gboolean input_freeze_correct_text_;

gulong cursor_position_handler_id = 0;

/*
 * Handler of cursor position change in the input buffer.
 * Its task is to keep insert and selection_bound marks
 * at the end of the input line.
 * This disables text selection
 * and setting cursor position in the middle of the input line,
 */
static void on_cursor_position_change(GObject *gobject, GParamSpec *pspec, gpointer user_data) {

	// Get cursor and selection marks
	GtkTextMark *insert_mark = gtk_text_buffer_get_insert(tb_input);
	GtkTextMark *selection_bound_mark = gtk_text_buffer_get_selection_bound(
			tb_input);

	// Get cursor and selection iterators
	GtkTextIter insert_iter, selection_bound_iter, eol_iter;
	gtk_text_buffer_get_iter_at_mark(tb_input, &insert_iter, insert_mark);
	gtk_text_buffer_get_iter_at_mark(tb_input, &selection_bound_iter,
			selection_bound_mark);
	gtk_text_buffer_get_end_iter(tb_input, &eol_iter);

	// Check if the iterators are the same
	gboolean cmp1 = gtk_text_iter_compare(&insert_iter, &eol_iter);
	gboolean cmp2 = gtk_text_iter_compare(&selection_bound_iter, &eol_iter);
	if (cmp1 != 0 || cmp2 != 0) {
		// Iterators are different.
		// Place both marks to the end of the input line.
		gtk_text_buffer_place_cursor(tb_input, &eol_iter);
	}
}

/*
 * Change the method of highlighting symbols in the input and sample textviews
 * See description in the gsettings schema
 */
void buffers_set_parameters(
		gboolean sample_grey_out,
		gboolean sample_highlight_current,
		gboolean sample_highlight_errors,
		gboolean input_highlight_errors,
		gboolean input_highlight_all_line,
		gboolean input_freeze_correct_text,
		gboolean input_keep_cursor_at_eol) {
	sample_grey_out_ = sample_grey_out;
	sample_highlight_current_ = sample_highlight_current;
	sample_highlight_errors_ = sample_highlight_errors;
	input_highlight_errors_ = input_highlight_errors;
	input_highlight_all_line_ = input_highlight_all_line;
	input_freeze_correct_text_ = input_freeze_correct_text;

	// Connect/disconnect a signal for cursor behavior in the input line.

	if (input_keep_cursor_at_eol && cursor_position_handler_id == 0) {

		// Constrain cursor position to always be at the end of input line
		cursor_position_handler_id = g_signal_connect(tb_input,
				"notify::cursor-position",
				G_CALLBACK (on_cursor_position_change), NULL);

	} else if (!input_keep_cursor_at_eol
			&& cursor_position_handler_id > 0) {

		// Disable constraints on cursor position
		g_signal_handler_disconnect(tb_input,
				cursor_position_handler_id);
		cursor_position_handler_id = 0;
	}
}

/*
 * Insert @text in the input buffer at current cursor position
 */
void buffers_input_insert_at_cursor(gchar *text) {
	gtk_text_buffer_insert_at_cursor(tb_input, text, strlen(text));
}

/*
 * Get first line of text from the buffer and transform it to ucs4 array
 *
 * @len (out) length of the ucs4 array
 * @return the ucs4 array
 */
static gchar * get_first_line(GtkTextBuffer *buffer) {
	GtkTextIter start, end;
	gtk_text_buffer_get_start_iter(buffer, &start);
	end = start;
	gtk_text_iter_forward_to_line_end (&end);
	return gtk_text_buffer_get_slice(buffer, &start, &end, FALSE);
}

/*
 * Get first line of text from the input buffer
 * and transform it to ucs4 array.
 *
 * @return the ucs4 array
 */
gchar * buffers_input_get_first_line() {
	return get_first_line(tb_input);
}

/*
 * Set tag by name between the offsets
 */
static void set_tag(GtkTextBuffer *buffer, gchar *tag_name, gint start_offset, gint end_offset) {
	GtkTextIter start, end;
	gtk_text_buffer_get_iter_at_offset(buffer, &start, start_offset);
	gtk_text_buffer_get_iter_at_offset(buffer, &end, end_offset);
	gtk_text_buffer_apply_tag_by_name(buffer, tag_name, &start, &end);
}

/*
 * Remove all tags between the offsets
 */
static void remove_all_tags(GtkTextBuffer *buffer, gint start_offset, gint end_offset) {
	GtkTextIter start, end;
	gtk_text_buffer_get_iter_at_offset(buffer, &start, start_offset);
	gtk_text_buffer_get_iter_at_offset(buffer, &end, end_offset);
	gtk_text_buffer_remove_all_tags(buffer, &start, &end);
}

/*
 * Get end offset of the first line
 */
static gint get_end_offset(GtkTextBuffer *buffer) {
	GtkTextIter start, end;
	gtk_text_buffer_get_start_iter(buffer, &start);
	end = start;
	gtk_text_iter_forward_to_line_end (&end);
	return gtk_text_iter_get_offset(&end);
}

/*
 * @pos the offset of the first difference between input and sample first lines
 * @len the length of the sample first line
 * @warning FALSE - use normal colors, TRUE - use warning colors
 */
void buffers_sample_set_tags(gint offset, gboolean warning) {
	gint end = get_end_offset(tb_sample);

	g_assert(0 <= offset && offset <= end);

	remove_all_tags(tb_sample, 0, end);

	if (sample_grey_out_)
		set_tag(tb_sample, SAMPLE_READY_COLOR, 0, offset);
	if (offset < end && sample_highlight_current_)
		set_tag(tb_sample, SAMPLE_CURRENT_COLOR, offset, offset + 1);
	if (warning && sample_highlight_errors_)
		set_tag(tb_sample, SAMPLE_WARNING_COLOR, offset, end);
}

/*
 * Set tags (color and readonly) in the input buffer
 *
 * @offset the offset of the first difference between input and sample first lines
 * @warning FALSE - use normal colors, TRUE - use warning colors
 */
void buffers_input_set_tags(gint offset, gboolean warning) {
	gint end = get_end_offset(tb_input);

	g_assert(0 <= offset && offset <= end);

	remove_all_tags(tb_input, 0, end);

	if (warning && input_highlight_errors_) {
		if (input_highlight_all_line_)
			set_tag(tb_input, INPUT_WARNING_COLOR, 0, end);
		else
			set_tag(tb_input, INPUT_WARNING_COLOR, offset, end);
	}

	// Freezing correct text in the input line
	if (input_freeze_correct_text_)
		set_tag(tb_input, INPUT_READONLY, 0, offset);
}


/*
 * Clear the input buffer.
 * This causes the call to on_input_buffer_changed().
 */
void buffers_input_clear() {
	//g_message("buffers_input_clear");

	gtk_text_buffer_set_text(tb_input, "", -1);
	buffers_input_set_tags(0, FALSE);
}

/*
 * Set the text in the sample buffer.
 * This causes the call to on_sample_buffer_changed().
 */
void buffers_sample_set_text(gchar *text) {
	//g_message("buffers_sample_set_text");

	gtk_text_buffer_set_text(tb_sample, text, -1);
}

/*
 * Remove symbols from the @offset to the end of the input buffer
 */
gboolean buffers_do_autobackspace(gpointer offset) {
	GtkTextIter start, end;
	gtk_text_buffer_get_iter_at_offset(tb_input, &start, *(gint*)offset);
	gtk_text_buffer_get_end_iter(tb_input, &end);
	gtk_text_buffer_delete(tb_input, &start, &end);
	return G_SOURCE_REMOVE;
}

/*
 * Handler of the input buffer change
 */
static void on_input_buffer_changed(GtkTextBuffer *tb_input, gpointer user_data) {
	lesson_input_buffer_changed();
}

/*
 * Handler of the sample buffer change
 */
static void on_sample_buffer_changed(GtkTextBuffer *tb_input, gpointer user_data) {
	lesson_sample_buffer_changed();
}

/*
 * Handler of the text insertion in the input buffer
 *
 * Divide all input characters in three groups:
 * GOOD: Correct character
 * BAD: First incorrect character after the correct one.
 * 	It is used in the error calculation.
 * SKIP: All other incorrect characters
 */
static void insert_text (GtkTextBuffer *textbuffer,
               GtkTextIter   *location,
               gchar         *text,
               gint           len,
               gpointer       user_data) {

	gint location_offset = gtk_text_iter_get_offset(location);
	lesson_input_buffer_insert_text(location_offset, text);
}

/*
 * Constructor
 */
void buffers_init(GtkTextBuffer *input, GtkTextBuffer *sample) {
	//g_message("buffers_init");

	tb_input = input;
	tb_sample = sample;

	// Create tags

	GdkColor color;

	gdk_color_parse("lightgrey", &color);
	gtk_text_buffer_create_tag(tb_sample, SAMPLE_READY_COLOR,
			"foreground-gdk", &color, NULL);

	gdk_color_parse("moccasin", &color);
	gtk_text_buffer_create_tag(tb_sample, SAMPLE_CURRENT_COLOR,
			"background-gdk", &color, NULL);

	gdk_color_parse("darkred", &color);
	gtk_text_buffer_create_tag(tb_sample, SAMPLE_WARNING_COLOR,
			"foreground-gdk", &color, NULL);

	gdk_color_parse("yellow", &color);
	gtk_text_buffer_create_tag(tb_input, INPUT_WARNING_COLOR, "background-gdk",
			&color, NULL);

	gtk_text_buffer_create_tag (tb_input, INPUT_READONLY, "editable", FALSE, NULL);

	// Connect signals
	g_signal_connect(tb_input, "insert-text", G_CALLBACK (insert_text),
			NULL);
	g_signal_connect(tb_input, "changed",
			G_CALLBACK (on_input_buffer_changed), NULL);
	g_signal_connect(tb_sample, "changed",
			G_CALLBACK (on_sample_buffer_changed), NULL);
}

/*
 * Destructor
 */
void buffers_finalize() {
	//g_message("buffers_finalize");
}
