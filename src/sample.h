#ifndef _SAMPLE_H_
#define _SAMPLE_H_

#include <glib.h>

// dependencies
#include "definitions.h"

void sample_init(gsize height, gsize width, gsize stepback, Generator generator);
gsize sample_get_height();
gchar * sample_get_text();
gchar * sample_get_first_line(glong *first_line_len);
void sample_shift();
void sample_finalize();

#endif /* _SAMPLE_H_ */
