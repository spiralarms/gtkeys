#ifndef _GTKEYS_APP_H_
#define _GTKEYS_APP_H_

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTKEYS_TYPE_APP             (gtkeys_app_get_type ())
#define GTKEYS_APP(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTKEYS_TYPE_APP, GtkeysApp))
#define GTKEYS_APP_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTKEYS_TYPE_APP, GtkeysAppClass))
#define GTKEYS_IS_APP(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTKEYS_TYPE_APP))
#define GTKEYS_IS_APP_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTKEYS_TYPE_APP))
#define GTKEYS_APP_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTKEYS_TYPE_APP, GtkeysAppClass))

typedef struct _GtkeysAppClass GtkeysAppClass;
typedef struct _GtkeysApp GtkeysApp;
typedef struct _GtkeysAppPrivate GtkeysAppPrivate;

struct _GtkeysAppClass {
	GtkApplicationClass parent_class;
};

struct _GtkeysApp {
	GtkApplication parent_instance;

	GtkeysAppPrivate *priv;
};

GType gtkeys_app_get_type(void) G_GNUC_CONST;

GtkeysApp * gtkeys_app_new(void);

G_END_DECLS

#endif /* _GTKEYS_APP_H_ */

