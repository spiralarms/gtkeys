#include "sample.h"

// dependencies

gsize height;
gsize width;
gsize stepback;
Generator generator;
GPtrArray *lines;

/*
 * Get the height of the sample widget = the number of lines in sample text
 */
gsize sample_get_height() {
	return height;
}

/*
 * The constructor of sample manager
 *
 * @height number of lines in sample widget = height of sample widget
 * @s_width max number of symbols in generated lines = width of sample widget
 * @s_stepback max number of symbols to step back during generation of accurate line endings
 * @s_generator initial generator of text lines
 */
void sample_init(gsize s_height, gsize s_width, gsize s_stepback, Generator s_generator) {
	//g_message("sample_init");

	gchar *line;

	g_assert(s_width > s_stepback);

	height = s_height;
	width = s_width;
	stepback = s_stepback;
	generator = s_generator;

	sample_finalize();
	lines = g_ptr_array_new();

	for (int i = 0; i < height; i++) {
		line = (*generator)(width, stepback, NULL);
		g_ptr_array_add(lines, (gpointer) line);
	}
}

/*
 * Get s_height lines of text for the sample buffer.
 * Every line has the length equal to the width of input buffer + 1
 * and determines the size of input textview.
 *
 * @sample_len (out) the length of the first line
 * @return (out) the text
 */
gchar * sample_get_text() {
	gchar *line;

	g_assert (lines != NULL && lines->len > 0);

	GString *sbuf = g_string_new("");

	for (int i = 0; i < lines->len; i++) {

		line = g_ptr_array_index(lines, i);
		g_string_append(sbuf, line);

		glong len = g_utf8_strlen(line, -1);

		// add spaces to make the number of characters
		// in the line equal to width + 1
		for(int j = 0; j < width - len + 1; j++)
			g_string_append(sbuf, " ");

		// add newline to all lines except the last one
		if (i < lines->len - 1)
			g_string_append(sbuf, "\n");
	}

	return g_string_free(sbuf, FALSE);
}

/*
 * Get the first line of the text for the sample buffer
 *
 * @sample_len (out) the length of the first line
 * @return (out) the line
 */
gchar * sample_get_first_line(glong *first_line_len) {
	g_assert (lines != NULL && lines->len > 0);
	g_assert (first_line_len != NULL);

	gchar *line = g_strdup(g_ptr_array_index(lines, 0));
	*first_line_len = g_utf8_strlen(line, -1);

	g_assert(*first_line_len <= width);

	return line;
}

/*
 * Shift the text for the input buffer one line up
 */
void sample_shift() {
	gchar *line;

	g_assert (lines != NULL && lines->len > 0);

	// delete first line
	g_ptr_array_remove_index(lines, 0);

	// append new line
	line = (*generator)(width, stepback, NULL);
	g_ptr_array_add(lines, (gpointer) line);
}

/*
 * Destructor
 */
void sample_finalize() {
	//g_message("sample_finalize");

	if (lines == NULL)
		return;

	for (int i = 0; i < lines->len; i++)
		g_free(g_ptr_array_index(lines, i));

	g_ptr_array_free(lines, TRUE);
	lines = NULL;
}
