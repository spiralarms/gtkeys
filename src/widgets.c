#include <glib/gi18n.h>
#include <gio/gio.h>
#include <cairo.h>
#include <math.h>

#include "config.h"
#include "widgets.h"

// dependencies
#include "gtkeys-app-prefs.h"
#include "lesson.h"


#define GETW(widget) widget = GTK_WIDGET(gtk_builder_get_object(builder, #widget))

#define GETWS(widget, signal) GETW(widget); \
	g_signal_connect(widget, #signal, G_CALLBACK (on_ ## widget ## _ ## signal), NULL)

#define GETWA(widget) GETWS(widget, activate)

GApplication *app;

GtkWidget *window;
GtkWidget *tv_sample;
GtkWidget *tv_input;

GtkWidget *item_usertext;
GtkWidget *item_userwords;
GtkWidget *item_fullscreen;
GtkWidget *item_leave_fullscreen;
GtkWidget *item_preferences;
GtkWidget *item_about;
GtkWidget *item_quit;

GtkWidget *item_en_oneword;
GtkWidget *item_en_words;
GtkWidget *item_en_text;
GtkWidget *item_ru_oneword;
GtkWidget *item_ru_words;
GtkWidget *item_ru_text;

GtkWidget *item_c;
GtkWidget *item_cpp;
GtkWidget *item_java;
GtkWidget *item_python;
GtkWidget *item_bash;

GtkWidget *tbtn_refresh;
GtkWidget *tool_autobackspace;
GtkWidget *tool_randomize;

GtkWidget *lb_lesson_title;

GtkWidget *lb_last_kpm;
GtkWidget *lb_last_cpm;
GtkWidget *lb_last_errors;
GtkWidget *lb_last_kspc;

GtkWidget *lb_lesson_kpm;
GtkWidget *lb_lesson_cpm;
GtkWidget *lb_lesson_errors;
GtkWidget *lb_lesson_kspc;

GtkWidget *darea_lesson;
GtkWidget *darea_last_line;

GtkWidget *pb_lesson_time;
GtkWidget *time_selector;

GtkeysAppPrefs *prefs;

GtkWidget * widgets_get_window() {
	return window;
}

GtkTextBuffer * widgets_get_input_buffer() {
	return gtk_text_view_get_buffer(GTK_TEXT_VIEW(tv_input));
}

GtkTextBuffer * widgets_get_sample_buffer() {
	return gtk_text_view_get_buffer(GTK_TEXT_VIEW(tv_sample));
}

void widgets_apply_font(gchar *fontname) {
	PangoFontDescription *font = pango_font_description_from_string(
			fontname);
	gtk_widget_override_font(tv_input, font);
	gtk_widget_override_font(tv_sample, font);
	pango_font_description_free(font);
}

/*
 * Pressing the button "Set Default" sets default values and shows the dialog again.
 * Pressing the buttons "OK" or "close window" hides the dialog and applies changes.
 */
void widgets_show_preferences() {
	gint response_id;

	while ((response_id = gtk_dialog_run(GTK_DIALOG (prefs))) == 2)
		gtkeys_app_prefs_set_default(prefs);

	gtk_widget_hide(GTK_WIDGET(prefs));
}

static void on_item_preferences_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_show_preferences();
}

static void on_time_selector_changed(GtkComboBox *widget, gpointer user_data) {
	const gchar *text = gtk_combo_box_get_active_id (GTK_COMBO_BOX(widget));
	gint lesson_time_min = g_ascii_strtoll (text, NULL, 10);
	lesson_set_time(lesson_time_min);
}

static void on_item_user_activate(GtkMenuItem *menuitem, gpointer user_data) {
	GtkWidget *dialog;
	gint res;

	dialog = gtk_file_chooser_dialog_new("Open File", GTK_WINDOW(window), GTK_FILE_CHOOSER_ACTION_OPEN,
			_("_Cancel"), GTK_RESPONSE_CANCEL, _("_Open"),
			GTK_RESPONSE_ACCEPT,
			NULL);

	res = gtk_dialog_run(GTK_DIALOG(dialog));

	if (res == GTK_RESPONSE_ACCEPT) {
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
		filename = gtk_file_chooser_get_filename(chooser);

		if (menuitem == GTK_MENU_ITEM(item_usertext))
			lesson_select(USER_TEXT, filename);
		else if (menuitem == GTK_MENU_ITEM(item_userwords))
			lesson_select(USER_WORDS, filename);

		g_free(filename);
	}

	gtk_widget_destroy(dialog);
}

static void on_item_fullscreen_activate(GtkMenuItem *menuitem, gpointer user_data) {
	gtk_window_fullscreen(GTK_WINDOW(window));
}

static void on_item_leave_fullscreen_activate(GtkMenuItem *menuitem, gpointer user_data) {
	gtk_window_unfullscreen(GTK_WINDOW(window));
}

static void on_item_quit_activate(GtkMenuItem *menuitem, gpointer user_data) {
	g_application_quit (G_APPLICATION (app));
}

static void on_item_en_oneword_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(EN_ONEWORD, NULL);
}

static void on_item_ru_oneword_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(RU_ONEWORD, NULL);
}

static void on_item_en_words_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(EN_WORDS, NULL);
}

static void on_item_ru_words_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(RU_WORDS, NULL);
}

static void on_item_en_text_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(EN_TEXT, NULL);
}

static void on_item_ru_text_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(RU_TEXT, NULL);
}


static void on_item_c_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(C_TEXT, NULL);
}

static void on_item_cpp_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(CPP_TEXT, NULL);
}

static void on_item_java_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(JAVA_TEXT, NULL);
}

static void on_item_python_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(PYTHON_TEXT, NULL);
}

static void on_item_bash_activate(GtkMenuItem *menuitem, gpointer user_data) {
	lesson_select(BASH_TEXT, NULL);
}

static void on_item_about_activate(GtkMenuItem *menuitem, gpointer user_data) {

	const char *authors[] = {
			"Serguei Yanush <s.yanush@gmail.com>",
			"Herman Yanush <herman.yanush2@gmail.com>",
			NULL };

	gtk_show_about_dialog (NULL,
	                       "program-name", PACKAGE_NAME,
			       "version", PACKAGE_VERSION,
			       "comments", "GTK+ Typing Tutor",
	                       "title", _("About GTKeys"),
			       "authors", authors,
			       "website", "https://bitbucket.org/spiralarms/gtkeys",
	                       NULL);
}

static gboolean on_tv_input_key_press_event(GtkWidget *widget, GdkEventKey *pKey,
		gpointer user_data) {
	return lesson_key_press_event(pKey);
}

static void on_tool_autobackspace_toggled(GtkToggleToolButton *toggle_tool_button,
		gpointer user_data) {
	gboolean active = gtk_toggle_tool_button_get_active(toggle_tool_button);
	lesson_autobackspace(active);
}

static void on_tool_randomize_toggled(GtkToggleToolButton *toggle_tool_button,
		gpointer user_data) {

	gboolean randomize = gtk_toggle_tool_button_get_active(toggle_tool_button);
	lesson_set_randomize(randomize);
}

static void on_tbtn_refresh_clicked(GtkToolButton *toolbutton, gpointer user_data) {
	lesson_refresh();
}

static gboolean on_draw_rounded_rectangle(GtkWidget *widget, cairo_t *cr,
                              gpointer user_data)
{
	gint width = gtk_widget_get_allocated_width(widget);
	gint height = gtk_widget_get_allocated_height(widget);

	//cairo_set_source_rgb(cr, 0.10, 0.88, 0.08);
	cairo_set_source_rgb(cr, 0.7, 0.7, 0.7);
	gint lw = 4;
	gint lw2 = lw/2;

	double radius = 30;
	double degrees = M_PI / 180.0;

	cairo_set_line_width (cr, lw);
	cairo_new_sub_path (cr);
	cairo_arc (cr, width - radius - lw2, radius + lw2, radius, -90 * degrees, 0 * degrees);
	cairo_arc (cr, width - radius - lw2, height - radius - lw2, radius, 0 * degrees, 90 * degrees);
	cairo_arc (cr, radius + lw2, height - radius - lw2, radius, 90 * degrees, 180 * degrees);
	cairo_arc (cr, radius + lw2, radius + lw2, radius, 180 * degrees, 270 * degrees);
	cairo_close_path (cr);
	cairo_stroke (cr);
	return FALSE;
}

/*
 * Constructor for widgets
 */
void widgets_init(GApplication *application) {
	//g_message("widgets_init");

	// for proper quit
	app = application;

	// Build interface
	GtkBuilder *builder = gtk_builder_new();
	GError* error = NULL;
	if (!gtk_builder_add_from_file(builder, WINDOW_UI, &error)) {
		g_critical("Couldn't load builder file: %s", error->message);
		g_error_free(error);
		gtk_main_quit();
		return;
	}

	// Get widgets

	GETW(window);
	gtk_window_set_title(GTK_WINDOW(window), PACKAGE_STRING);

	GETW(tv_input);
	g_signal_connect(tv_input, "key-press-event", G_CALLBACK (on_tv_input_key_press_event), NULL);

	GETW(tv_sample);

	GETW(lb_lesson_title);
	GETW(lb_last_kpm);
	GETW(lb_last_cpm);
	GETW(lb_last_errors);
	GETW(lb_last_kspc);
	GETW(lb_lesson_kpm);
	GETW(lb_lesson_cpm);
	GETW(lb_lesson_errors);
	GETW(lb_lesson_kspc);

	GETW(pb_lesson_time);
	gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(pb_lesson_time), TRUE);

	GETWS(tool_autobackspace, toggled);
	gtk_toggle_tool_button_set_active (GTK_TOGGLE_TOOL_BUTTON(tool_autobackspace), FALSE);

	GETWS(tool_randomize, toggled);
	gtk_toggle_tool_button_set_active (GTK_TOGGLE_TOOL_BUTTON(tool_randomize), TRUE);

	GETWS(tbtn_refresh, clicked);

	GETWS(time_selector, changed);

	GETW(item_usertext);
	g_signal_connect(item_usertext, "activate", G_CALLBACK (on_item_user_activate), NULL);

	GETW(item_userwords);
	g_signal_connect(item_userwords, "activate", G_CALLBACK (on_item_user_activate), NULL);

	GETWA(item_fullscreen);
	GETWA(item_leave_fullscreen);
	GETWA(item_preferences);
	GETWA(item_about);
	GETWA(item_quit);

	GETWA(item_en_oneword);
	GETWA(item_en_words);
	GETWA(item_en_text);
	GETWA(item_ru_oneword);
	GETWA(item_ru_words);
	GETWA(item_ru_text);

	GETWA(item_c);
	GETWA(item_cpp);
	GETWA(item_java);
	GETWA(item_python);
	GETWA(item_bash);

	GETW(darea_lesson);
	GETW(darea_last_line);

	gtk_builder_connect_signals(builder, NULL);

	g_object_unref(builder);

	gtk_widget_grab_focus(tv_input);

	// GtkeysAppPrefs object has a pointer to GSettings object
	prefs = gtkeys_app_prefs_new(GTK_WINDOW(window));
	GSettings *settings = gtkeys_app_prefs_get_settings(prefs);

	lesson_init(settings);

	// Set time selector to default lesson time.
	gint default_id = g_settings_get_int(settings,
			TIME_SELECTOR_DEFAULT_ID_KEY);
	gtk_combo_box_set_active(GTK_COMBO_BOX(time_selector),
			default_id);

	// Draw rounded boxes
	g_signal_connect(G_OBJECT(darea_lesson), "draw",
	                 G_CALLBACK(on_draw_rounded_rectangle), NULL);
	g_signal_connect(G_OBJECT(darea_last_line), "draw",
	                 G_CALLBACK(on_draw_rounded_rectangle), NULL);

	// Show window.
	gtk_window_set_application(GTK_WINDOW(window),
				GTK_APPLICATION(app));
	gtk_widget_show_all(window);
}

/*
 * Set formatted text on label
 */
static void set_label(GtkLabel *label, gchar *format, gdouble data) {
	gchar *text = g_strdup_printf(format, data);
	gtk_label_set_text(label, text);
	g_free(text);
}

/*
 * Update statistical info and time progressbar
 */
void widgets_update_statdata(LessonData *ld, StatData *sd) {
	//g_message("widgets_update_statdata");

	gchar *text = "";
	switch (ld->current_lesson) {
		case EN_ONEWORD: text = _("English: One word"); break;
		case EN_WORDS: text = _("English: Words"); break;
		case EN_TEXT: text = _("English: Text"); break;
		case RU_ONEWORD: text = _("Russian: One word"); break;
		case RU_WORDS: text = _("Russian: Words"); break;
		case RU_TEXT: text = _("Russian: Text"); break;
		case C_TEXT: text = _("Programming: C"); break;
		case CPP_TEXT: text = _("Programming: C++"); break;
		case JAVA_TEXT: text = _("Programming: Java"); break;
		case PYTHON_TEXT: text = _("Programming: Python"); break;
		case BASH_TEXT: text = _("Programming: Bash"); break;
		case USER_TEXT: text = _("User Text"); break;
		case USER_WORDS: text = _("User Words"); break;
	default:;
	}

	gtk_label_set_text(GTK_LABEL(lb_lesson_title), text);

	set_label(GTK_LABEL(lb_last_kpm), "%.0f", sd->last_line.metrics.keystrokes_per_min);
	set_label(GTK_LABEL(lb_last_cpm), "%.0f", sd->last_line.metrics.characters_per_min);
	set_label(GTK_LABEL(lb_last_errors), "%.2f%%", sd->last_line.metrics.total_error_rate);
	set_label(GTK_LABEL(lb_last_kspc), "%.2f", sd->last_line.metrics.keystrokes_per_character);

	set_label(GTK_LABEL(lb_lesson_kpm), "%.0f", sd->lesson.metrics.keystrokes_per_min);
	set_label(GTK_LABEL(lb_lesson_cpm), "%.0f", sd->lesson.metrics.characters_per_min);
	set_label(GTK_LABEL(lb_lesson_errors), "%.2f%%", sd->lesson.metrics.total_error_rate);
	set_label(GTK_LABEL(lb_lesson_kspc), "%.2f", sd->lesson.metrics.keystrokes_per_character);

	gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(pb_lesson_time),
			ld->lesson_time_fraction);

	gdouble lesson_time = sd->lesson.counters.input_time;
	gint min = (int) lesson_time / 60;
	gint sec = (int) lesson_time - min * 60;
	gchar *time_text = g_strdup_printf("%d:%02d", min, sec);
	gtk_progress_bar_set_text(GTK_PROGRESS_BAR(pb_lesson_time), time_text);
	g_free(time_text);
}

/*
 * Destructor
 */
void widgets_finalize() {
	//g_message("widgets_finalize");

	gtk_widget_destroy(GTK_WIDGET(prefs));
	lesson_finalize();
}

/*
 * Set sensitivity of the textviews
 */
void widgets_textviews_set_sensitive(gboolean sensitive) {
	gtk_widget_set_sensitive(tv_input, sensitive);
	gtk_widget_set_sensitive(tv_sample, sensitive);
}

